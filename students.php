<?php

require_once 'workers.php';

class student extends workers
{
    private $course;

        public function getCourse()
        {
           return $this->course;
        }

        public function setCourse($course)
        {
            $this->course = $course;
        }

         public function addOneYear()
        {
            $this->age++;
        }

        public function setAge($age)
        {
            if ($age <= 25)
            {
                parent::setAge($age);
            }
        }

}

$stunent = new student();
$stunent->setName('Алексей');
$stunent->setAge(25);
$stunent->setSalary(1000);
$stunent->setCourse(' 3-м курсе');

echo 'Его зовут '.$stunent->getName().' , его зарпалата составляет '.$stunent->getSalary().' ему '.$stunent->getAge().' и он учится на'.$stunent->getCourse().'<br>';
