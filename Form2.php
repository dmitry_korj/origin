<?php


class Form2
{

    private function getAttr($attr)
    {
        $str = '';

        foreach ($attr as $key => $value)
        {
            $str = $key.'='.$value;
        }
            return $str;

    }

        public function input($attr)
        {
            $attr = $this->getAttr($attr);
            return '<input '.$attr.' /><br>';
        }

    public function close()
    {

        return '</form>';
    }

    public function open($attr)
    {
        $attr = $this->getAttr($attr);
        return '<form '.$attr.'>';
    }

    public function submit($attr)
    {
        $attr = $this->getAttr($attr);
        return '<input type="submit" '.$attr. ' /><br>';

    }

    public function pass($attr)
    {
        $attr = $this->getAttr($attr);
        return '<input type="text" '.$attr.'><br>';
    }


}
$form = new Form2();
echo $form->open(['action'=>'index.php', 'method'=>'POST']);
echo $form->input(['type'=>'text', 'placeholder'=>"Имя",]);
echo $form->pass(['placeholder'=>"Ваш пароль", 'name'=>'pass']);
echo $form->submit(['value'=>'Отправить']);
echo $form->close();