<?php


class methods
{

    public $a;

    public static $infoDeliveryEkaterinburg = [
        'template' => 'Мой шаблон',
        'street' => 'Ильича',
        'building' => '49',
        'flat' => '21',
        'postcode' => '620012'

    ];

    public static function fillDeliveryTemplateFields($array, \AcceptanceTester $I)
    {
        $I->fillField('#delivery-address-name', $array['template']);
        $I->fillField('#delivery-address-street', $array['street']);
        $I->fillField('#delivery-address-house_num', $array['building']);
        $I->fillField('#delivery-address-flat_num', $array['flat']);
        $I->fillField('#delivery-address-postal_code', $array['postcode']);
    }


    public static function checkImg(\AcceptanceTester $I)
    {
        //while ($I->dontSeeElement('span.b-circle__icon > img[src="/image/lesh-guest.jpg"]') == true)
        while ($I->cantSee('2 товара') == false)
        {

            $I->click('#login-form > div:nth-child(4) > div.auth_button > input');

            if ($I->cantSee('2','#user_page > div.layout > header > div > div.nav-bar > div.b-circle.cart-header > a.b-circle__inner.cart-header-fill > div > p > span.b > span.count'))
            {

            }
        }

    }

}
