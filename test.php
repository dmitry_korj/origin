<?php

Class Person
{
    private $name = 'Олег';
    protected $age;


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        if ($this->checkAge($age))
        {
            $this->age = $age;
        }
    }

    private function checkAge($age)
    {
        if ($age < 100)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    }



